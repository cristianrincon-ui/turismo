var xs = false;
var sm = false;
var md = false;
var lg = false;

(function($, viewport){
    $(document).ready(function() {

        if(viewport.is('xs')) {
        	xs = true;
        	sm = false;
        	md = false;
        	lg = false;
        	console.log("XS")
        } else if(viewport.is('sm')) {
        	sm = true;
        	xs = false;
        	md = false;
        	lg = false;
        	console.log("SM")
        } else if(viewport.is('md')) {
           	md = true;
           	xs = false;
           	sm = false;
           	lg = false;
           	console.log("MD")
        } else if (viewport.is('lg')) {
        	lg = true;
        	xs = false;
        	sm = false;
        	md = false;
        	console.log("LG")
        }

        $(window).resize(
            viewport.changed(function() {
                if(viewport.is('xs')) {
                	xs = true;
                	sm = false;
                	md = false;
                	lg = false;
                	console.log("XS")
                } else if(viewport.is('sm')) {
                	sm = true;
                	xs = false;
                	md = false;
                	lg = false;
                	console.log("SM")
                } else if(viewport.is('md')) {
                   	md = true;
                   	xs = false;
                   	sm = false;
                   	lg = false;
                   	console.log("MD")
                } else if (viewport.is('lg')) {
                	lg = true;
                	xs = false;
                	sm = false;
                	md = false;
                	console.log("LG")
                }
            })
        );
    });
})(jQuery, ResponsiveBootstrapToolkit);


jQuery(document).ready(function($) {

	chargeContent("main");

	// Animations
	//-----------------------------------------------
	var delay=0, setTimeoutConst;
	if (($("[data-animation-effect]").length>0) && !Modernizr.touch) {
		$("[data-animation-effect]").each(function() {
			var item = $(this),
			animationEffect = item.attr("data-animation-effect");

			if(Modernizr.mq('only all and (min-width: 768px)') && Modernizr.csstransitions) {
				item.appear(function() {
					if(item.attr("data-effect-delay")) item.css("effect-delay", delay + "ms");
					setTimeout(function() {
						item.addClass('animated object-visible ' + animationEffect);

					}, item.attr("data-effect-delay"));
				}, {accX: 0, accY: -130});
			} else {
				item.addClass('object-visible');
			}
		});
	};

	// Hide Submenu
	//-----------------------------------------------
	$(document).on('click', function(e) {

		if ($(".sub-menu.fixed li.active")) {

			if ((xs == true) || (sm == true)) {
			} else {
				hideSubmenu();
			}

		} else {
			hideSubmenu();
		}
	});

	// Main Menu
	//-----------------------------------------------
	$(".menu-content > li").on('click', function(e) {
		var key = $(this).attr('data-key');

		e.stopPropagation();
		showSubmenu($(this), key);

		if ((xs == true && $(this).hasClass('sub')) || (sm == true && $(this).hasClass('sub'))) {
			e.stopPropagation();
		} else {
			if (xs == true) {
				hideMenu();
			} else {
				
			}
		}

	});

	// Submenu (fixed)
	//-----------------------------------------------
	$(".sub-menu.fixed > li").on('click', function(e) {
		var key = $(this).attr('data-key');

		if (xs == true) {
		} else {
			e.stopPropagation();
		}

		if (xs == true) {
			toggleMenu();
		} else if(md == true) {
			$("#main").removeClass('col-md-10 col-md-offset-2').addClass('col-md-8 col-md-offset-4');
		} else if(lg == true) {
			$("#main").removeClass('col-lg-10 col-lg-offset-2').addClass('col-lg-9 col-lg-offset-3');
		}

		$(".sub-menu.fixed > li").removeClass('active');
		$(this).addClass('active');
		chargeContent(key);
	});

	// Submenu
	//-----------------------------------------------
	$(".sub-menu > li").on('click', function(e) {
		var key = $(this).attr('data-key');

		if (xs == true) {
		} else {
			e.stopPropagation();
		}

		if (xs == true) {
			toggleMenu();
		}

		hideSubmenu();
		$(".sub-menu > li").removeClass('active');
		$(this).addClass('active');
		chargeContent(key);
	});
	
});

function showSubmenu(me, key) {

	if (xs == true) {
	} else if(md == true) {
		$("#main").addClass('col-md-10 col-md-offset-2').removeClass('col-md-8 col-md-offset-4');
	} else if(lg == true) {
		$("#main").addClass('col-lg-10 col-lg-offset-2').removeClass('col-lg-9 col-lg-offset-3');
	}

	me.addClass('active').siblings().removeClass('active');
	$(".sub-menu > li").removeClass('active')
	$(".sub-menu").addClass('slideOutLeft').removeClass('slideInLeft');

	if (me.hasClass('sub')) {
		var key = me.attr('data-key');
		var target = $(".sub-menu[data-key='"+key+"']");

		setTimeout(function() {
			$(".sub-menu").not("[data-key='"+key+"']").hide();
		}, 300)
		target.addClass('animated slideInLeft').removeClass('slideOutLeft').show();
	}

	if (key == 2) {
		chargeContent("main");
	} else if (key == 6) {
		chargeContent("contacto");
	}
}

function hideSubmenu() {
	$(".sub-menu").addClass('slideOutLeft').removeClass('slideInLeft');

	if (xs == true) {
	} else if(md == true) {
		$("#main").addClass('col-md-10 col-md-offset-2').removeClass('col-md-8 col-md-offset-4');
	} else if(lg == true) {
		$("#main").addClass('col-lg-10 col-lg-offset-2').removeClass('col-lg-9 col-lg-offset-3');
	}
}

function chargeContent(key) {
	var actualContent = $("#main").html();

	$("#main").data('html', actualContent)
	$("#main").load("modules/"+key+".html");
}

function toggleMenu(e) {
	var target = $("#side-menu")
	var active = $("#side-menu").hasClass('active');
	var button = $(".header span");

	if (active && xs == true) {
		target.addClass('animated slideOutLeft').removeClass('active slideInLeft hidden-xs').show();
		button.addClass('icon-menu').removeClass('icon-cross');
	} else {
		target.addClass('animated slideInLeft active').removeClass('slideOutLeft hidden-xs').show();
		button.removeClass('icon-menu').addClass('icon-cross');
	}
}

function showMenu() {
	var target = $("#side-menu")
	var button = $(".header span");

	target.addClass('animated slideInLeft active').removeClass('slideOutLeft hidden-xs').show();
	button.removeClass('icon-menu').addClass('icon-cross');
}

function hideMenu() {
	var target = $("#side-menu")
	var button = $(".header span");

	target.addClass('animated slideOutLeft').removeClass('active slideInLeft hidden-xs').show();
	button.addClass('icon-menu').removeClass('icon-cross');
}

function scrollGo(me) {
	var target = me.attr('data-target');
	console.log(target);
	$("html, body").animate({ scrollTop: $('[data-id="'+target+'"]').offset().top - 50 }, 1000);
}